//Direction:
//Create 2 event listeners when a user types in the first and last name inputs
//When this event triggers, update the span-full-name's content to show the value of the first name input on th right, go  to HTML and change the name.
//Stretch goal: Instead of an anonyomous function, create a new function that the two event listens will call
//Guide question: Where do the names come from and where should they go? clue: content .dir

//1 Create 2 event listeners when a user types in the first and last name inputs
//The value refers to the value name name sa console

//====================================================
let name = document.querySelector('#txt-first-name'); 
let surName = document.querySelector('#txt-lastname-name');
let fullName = document.querySelector('#span-full-name');


let updateName = (input1, input2) => {
	let nameInput = input1.value;
	let surNameInput = input2.value;
	fullName.innerHTML = `${nameInput} ${surNameInput}` 
}



name.addEventListener("keyup", () => {
updateName(name, surName);//displays input 1 on console.log
})

surName.addEventListener("keyup", updateName)

//------------------------------------------------


//SIR JINO'S ANSWER
//============================================================
// let input1 = document.querySelector('#txt-first-name'); 
// let input2 = document.querySelector('#txt-lastname-name');
// let span1 = document.querySelector('#span-full-name');


// const updateTheName = () => {
// 	let name = input1.value;
// 	let lastname = input2.value;
// 	span1.innerHTML = `${name} ${lastname}` //re assignment
// }


// input1.addEventListener("keyup", () => {
// 	updateTheName() //displays input 1 on console.log	
// })

// input2.addEventListener("keyup", updateTheName)


