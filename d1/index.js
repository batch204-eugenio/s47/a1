// # ID selector
//document refers to the HTML file linked to this JS file
//querySelector allows us to designate an HTML element that we want to access via its ID or CLASS name.

//4 ways to access an element.
let input1 = document.querySelector('#txt-first-name'); //dot notation 'querySelector' is a method.
let span1 = document.querySelector('#span-full-name');

	document.getElementByID('txt-first-name');
	document.getElementByClassName('txt-inputs');
	document.getElementByTagName('input');

	console.log(input1);
//or
	console.dir(input1);     	 	// .dir command looks at more details than .log
	console.log(input1.value) 	// .value to access the value on console.
	//input1.value = "Jack"; 		//check html line 9 from "Mark" to "Jack"

	console.dir(span1)
	console.log(span1.textContent) 	//ALL text, including whitespaces
	console.log(span1.innerHTML) 	//ALL HTML and text within the given HTML element.
	console.log(span1.innerText) 	//Just the text



//Events and Event Listeners:
//An event is ANY kind of user interaction with a specific element on a web page (clicking a button, typing in an input, etc).
//Events have targets, which are the specific elements being interacted with.

let button = document.getElementById('button')
//An event listener waits for a given event to occur before code execution.
button.addEventListener('click', () => { 		//'click' is an event and not user define.
	alert("You clicked it!")
}) 
//try to click the button sa form sa console.log
//----------------------------------------------------
//GETTING what our user type in the form input real time.
//this data input1 is called out from HTML
let input1 = document.querySelector('#txt-first-name'); //dot notation 'querySelector' is a method.

//1st option same result as option 2 but would only work for a specic input.
input1.addEventListener("keyup", () => {
	console.log(input1.value);  //displays input 1 on console.log
}) //type your name sa FIRST NAME place holder sa form sa console. //whenever you type, tuloy tuloy din sya sa console.log

//2nd option same result as option 1 but much flexible and efficient, works in any and all input
input1.addEventListener("keyup", () => {
	console.log(event.target.value)  //same as console.log(input1.value) 
}) //type your name sa FIRST NAME place holder sa form sa console. //whenever you type, tuloy tuloy din sya sa console.log and will double.

//2nd option shorthand much preferred in real world.
input1.addEventListener("keyup", (e) => { //e is not user define as a parameter
	console.log(e.target.value)  //same as console.log(input1.value) 
}) //type your name sa FIRST NAME place holder sa form sa console. //whenever you type, tuloy tuloy din sya sa console.log and will double.
